export function success(body) {
    return buildResponse(200, body);
  }
  export function failure(body) {
    return buildResponse(500, body);
  }
  function buildResponse(statusCode, body) {
    console.log("******error in create - response lib");
    console.log(JSON.stringify(body));
    return {
      statusCode: statusCode,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, HEAD, PATCH, OPTIONS'
      },
      body: JSON.stringify(body)
    };
  }